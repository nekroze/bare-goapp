FROM golang:1.9 as builder
WORKDIR /go/src/app/
COPY . .
RUN go get -d
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

#FROM alpine:latest
FROM scratch as deploy
# The following run is required if you would like to verify certificates against global CA's
#RUN apk --no-cache add ca-certificates
WORKDIR /
COPY --from=builder /go/src/app/app /
ENTRYPOINT ["/app"]
